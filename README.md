### AUTO SERVICE

---
###### Gateway AWS: http://18.228.61.168:5000
###### Service Discovery AWS: http://18.228.61.168:8761

---
###### Collection Postman: POSTMAN/caseauto.pm_collection.json
###### Environment Postman: POSTMAN/caseauto.pm_environment.json
###### Test Run Postman: POSTMAN/caseauto.pm_test_run.json

---
#### OAuth Token
[Gera novo token](#post-authoauthtoken) <br/>

#### Automóveis
[Cadastra novo automóvel](#post-autov1automoveis) <br/>
[Busca todos automóveis](#get-autov1automoveis) <br/>

#### Boletos
[Cadastra novo boleto](#post-autov1boletos) <br/>

---
### POST /auth/oauth/token
**Exemplo requisicao POSTMAN**
```

authorization {
    TYPE: Basic Auth
    Username: auto1
    Password: auto1
}

body {
    ----- form-data -----
    grant_type: password
    username  : Pedro
    password  : 12345
    ---------------------
}

```
**Exemplo resposta [200 OK]**
```
{
    "access_token": "2b45dd20-ff61-4e8a-96e4-51c3f8682a57",
    "token_type": "bearer",
    "refresh_token": "81c60163-141e-4c84-95be-f83632ff860b",
    "expires_in": 41479,
    "scope": "all"
}
```
---
### POST /auto/v1/automoveis
**Exemplo requisicao**
```
{
    "marca": "Fiat",
    "modelo": "Uno",
    "valor": "20.000,00",
    "dataCadastro": "01/01/2020"
}
```
**Exemplo resposta [201 Created]**
```
{
    "id": 2,
    "marca": "Fiat",
    "modelo": "Uno",
    "valor": "20.000,00",
    "dataCadastro": "01/10/2020"
}
```
### GET /auto/v1/automoveis
**Exemplo resposta [200 OK]**
```
[
    {
        "id": 1,
        "marca": "Volkswagen",
        "modelo": "Gol",
        "valor": "25.000,00",
        "dataCadastro": "01/09/2020",
        "links": []
    },
    {
        "id": 2,
        "marca": "Fiat",
        "modelo": "Uno",
        "valor": "20.000,00",
        "dataCadastro": "01/10/2020",
        "links": []
    }
]
```
---
### POST /auto/v1/boletos
**Exemplo requisicao**
```
{
    "marca": "Chevrolet",
    "modelo": "Onix",
    "valor": "30.000,00",
    "dataVencimento": "01/11/2020"
}
```
**Exemplo resposta [201 Created]**
```
{
    "codigo": 6,
    "valor": "30.000,00",
    "dataVencimento": "01/11/2020"
}
```