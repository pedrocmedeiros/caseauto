package com.itau.caseauto.repository;

import com.itau.caseauto.model.Boleto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class BoletoRepositoryTest {

    @Autowired
    private TestEntityManager manager;

    @Autowired
    private BoletoRepository repository;

    @Before
    public void setUp() {
        manager.clear();
    }

    @Test
    public void testCriarEBuscarBoleto() {
        Boleto boleto = new Boleto();
        boleto.setValor("200");

        manager.persist(boleto);
        manager.flush();

        Boleto buscado = repository.findAll().get(0);

        assertEquals(boleto.getValor(), buscado.getValor());
    }

    @Test
    public void testCriarEBuscarVariosBoletos() {
        Boleto boleto1 = new Boleto();
        boleto1.setValor("200");

        Boleto boleto2 = new Boleto();
        boleto2.setValor("200");

        manager.persist(boleto1);
        manager.persist(boleto2);
        manager.flush();

        List<Boleto> boletos = repository.findAll();

        assertEquals(2, boletos.size());
        assertEquals(boleto1.getValor(), boletos.get(0).getValor());
        assertEquals(boleto2.getValor(), boletos.get(1).getValor());
    }

    @Test
    public void testBuscarListaVaziaBoletos() {
        List<Boleto> boletos = repository.findAll();

        assertEquals(0, boletos.size());
    }
}