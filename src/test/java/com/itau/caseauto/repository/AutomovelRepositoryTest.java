package com.itau.caseauto.repository;

import com.itau.caseauto.model.Automovel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AutomovelRepositoryTest {

    @Autowired
    private TestEntityManager manager;

    @Autowired
    private AutomovelRepository repository;

    @Before
    public void setUp() {
        manager.clear();
    }

    @Test
    public void testCriarEBuscarAutomovel() {
        Automovel automovel = new Automovel();
        automovel.setMarca("Abc");

        manager.persist(automovel);
        manager.flush();

        Automovel buscado = repository.findAll().get(0);

        assertEquals(automovel.getMarca(), buscado.getMarca());
    }

    @Test
    public void testCriarEBuscarVariosAutomoveis() {
        Automovel automovel1 = new Automovel();
        automovel1.setMarca("auto1");

        Automovel automovel2 = new Automovel();
        automovel2.setMarca("auto2");

        manager.persist(automovel1);
        manager.persist(automovel2);
        manager.flush();

        List<Automovel> automoveis = repository.findAll();

        assertEquals(2, automoveis.size());
        assertEquals(automovel1.getMarca(), automoveis.get(0).getMarca());
        assertEquals(automovel2.getMarca(), automoveis.get(1).getMarca());
    }

    @Test
    public void testBuscarListaVaziaAutomoveis() {
        List<Automovel> automoveis = repository.findAll();

        assertEquals(0, automoveis.size());
    }
}