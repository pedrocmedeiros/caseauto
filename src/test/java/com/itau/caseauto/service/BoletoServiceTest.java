package com.itau.caseauto.service;

import com.itau.caseauto.model.Boleto;
import com.itau.caseauto.repository.BoletoRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class BoletoServiceTest {

    @TestConfiguration
    static class BoletoServiceTestContextConfiguration {

        @Bean
        public BoletoService boletoService() {
            return new BoletoService();
        }
    }

    @Autowired
    private BoletoService service;

    @MockBean
    private BoletoRepository repository;

    @Test
    public void testCriarBoleto() {
        Boleto boleto = new Boleto();
        boleto.setValor("200");

        when(repository.save(boleto))
                .thenReturn(boleto);

        Boleto criado = service.criar(boleto);

        assertEquals(boleto.getValor(), criado.getValor());
    }

}