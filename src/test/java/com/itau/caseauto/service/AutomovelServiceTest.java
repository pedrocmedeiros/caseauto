package com.itau.caseauto.service;

import com.itau.caseauto.model.Automovel;
import com.itau.caseauto.repository.AutomovelRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class AutomovelServiceTest {

    @TestConfiguration
    static class AutomovelServiceTestContextConfiguration {

        @Bean
        public AutomovelService automovelService() {
            return new AutomovelService();
        }
    }

    @Autowired
    private AutomovelService service;

    @MockBean
    private AutomovelRepository repository;


    @Test
    public void testCriarAutomovel() {
        Automovel automovel = new Automovel();
        automovel.setMarca("Abc");

        when(repository.save(automovel))
                .thenReturn(automovel);

        Automovel criado = service.criar(automovel);

        assertEquals(automovel.getMarca(), criado.getMarca());
    }

    @Test
    public void testBuscarAutomovel() {
        Automovel automovel = new Automovel();
        automovel.setMarca("Abc");

        when(repository.findAll())
                .thenReturn(Collections.singletonList(automovel));

        Automovel buscado = service.buscar().get(0);

        assertEquals(automovel.getMarca(), buscado.getMarca());
    }

    @Test
    public void testBuscarVariosAutomoveis() {
        Automovel automovel1 = new Automovel();
        automovel1.setMarca("auto1");
        Automovel automovel2 = new Automovel();
        automovel2.setMarca("auto1");

        when(repository.findAll())
                .thenReturn(Arrays.asList(automovel1, automovel2));

        List<Automovel> automoveis = repository.findAll();
        Automovel buscado1 = automoveis.get(0);
        Automovel buscado2 = automoveis.get(1);

        assertEquals(2, automoveis.size());
        assertEquals(automovel1.getMarca(), buscado1.getMarca());
        assertEquals(automovel2.getMarca(), buscado2.getMarca());
    }

    @Test
    public void testBuscarListaVaziaAutomoveis() {

        when(repository.findAll())
                .thenReturn(Collections.emptyList());

        List<Automovel> automoveis = service.buscar();

        assertEquals(0, automoveis.size());
    }

}