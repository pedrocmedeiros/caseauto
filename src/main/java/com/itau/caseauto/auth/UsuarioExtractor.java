package com.itau.caseauto.auth;

import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;

import java.util.Map;


public class UsuarioExtractor implements PrincipalExtractor {

    @Override
    public Object extractPrincipal(Map<String, Object> map) {
        Usuario usuario = new Usuario();
        usuario.setId(String.valueOf(map.get("id")));
        usuario.setName(String.valueOf(map.get("name")));
        return usuario;
    }
}
