package com.itau.caseauto.service;

import com.itau.caseauto.model.Automovel;
import com.itau.caseauto.repository.AutomovelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AutomovelService {

    @Autowired
    private AutomovelRepository repository;

    public Automovel criar(Automovel novoAutomovel) {
        return repository.save(novoAutomovel);
    }

    public List<Automovel> buscar() {
        return repository.findAll();
    }
}
