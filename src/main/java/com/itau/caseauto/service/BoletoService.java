package com.itau.caseauto.service;

import com.itau.caseauto.model.Boleto;
import com.itau.caseauto.repository.BoletoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BoletoService {

    @Autowired
    private BoletoRepository repository;

    public Boleto criar(Boleto novoBoleto) {
        return repository.save(novoBoleto);
    }
}
