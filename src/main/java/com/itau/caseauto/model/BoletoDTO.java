package com.itau.caseauto.model;

import org.springframework.hateoas.RepresentationModel;

public class BoletoDTO extends RepresentationModel<BoletoDTO> {

    private Long codigo;
    private String valor;
    private String dataVencimento;

    public BoletoDTO(Boleto boleto) {
        this.codigo = boleto.getCodigo();
        this.valor = boleto.getValor();
        this.dataVencimento = boleto.getDataVencimento();
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(String dataVencimento) {
        this.dataVencimento = dataVencimento;
    }
}
