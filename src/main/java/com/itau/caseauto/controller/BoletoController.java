package com.itau.caseauto.controller;

import com.itau.caseauto.hateoas.ResourceUtils;
import com.itau.caseauto.model.Boleto;
import com.itau.caseauto.model.BoletoDTO;
import com.itau.caseauto.service.BoletoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/v1/boletos")
public class BoletoController {

    @Autowired
    private BoletoService boletoService;

    @PostMapping
    public ResponseEntity<BoletoDTO> gerar(@Valid @RequestBody Boleto novoBoleto) {
        BoletoDTO boletoCriadoDTO = new BoletoDTO(boletoService.criar(novoBoleto));
        URI uri = new ResourceUtils<BoletoDTO>().criarURI(boletoCriadoDTO);
        return ResponseEntity.created(uri).body(boletoCriadoDTO);
    }
}
