package com.itau.caseauto.controller;

import com.itau.caseauto.hateoas.ResourceUtils;
import com.itau.caseauto.model.Automovel;
import com.itau.caseauto.service.AutomovelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/automoveis")
public class AutomovelController {

    @Autowired
    private AutomovelService autService;

    @PostMapping
    public ResponseEntity<Automovel> cadastrar(@Valid @RequestBody Automovel novoAutomovel) {
        Automovel automovelCriado = autService.criar(novoAutomovel);
        URI uri = new ResourceUtils<Automovel>().criarURI(automovelCriado);
        return ResponseEntity.created(uri).body(automovelCriado);
    }

    @GetMapping
    public ResponseEntity<List<Automovel>> buscarTodos() {
        return ResponseEntity.ok().body(autService.buscar());
    }
}
